# Lead!Tools
This application provides a collection of tools with integration with Office365 with the aim of streamlining the workflow of Student Leaders in NUSH.

## Features
- Dashboard with tool widgets
- Collection of productivity tools
  - Simple Calendar
  - Manpower Allocations
  - Inventory Manager
- Auto updater

## Installation
Download the latest Lead!Tools.jar file [here](http://parangninja.sytes.net/projects/Lead!Tools/latestVersion)

## How to use?
Double click to run the jar file or type the following in a terminal `java -jar Lead!Tools.jar`.

## Credits
NUS High School CS3233 Module Notes

## License
MIT (c) Li Yue Chen
