package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import controllers.AuthenticationController;
import controllers.core.AutoUpdater;
import controllers.core.SettingsManager;
import controllers.core.ToolManager;
import controllers.core.WindowManager;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import views.SplashScreen;

public class Main extends Application {
	private static Main application = null;
	private AutoUpdater autoUpdater = null;
	
	private WindowManager windowManager = null;
	private ToolManager toolManager = null;
	private AuthenticationController authenticator = null;
	
	// BLOCK: Settings
	private SettingsManager settingsManager = null;	// TODO: Centralize all into this
	private Locale locale = null;
	private ResourceBundle display = null;
	
	public static final String APPLICATION_NAME = "Lead! Tools";
	public static final String APPLICATION_ICON = "/resources/App_Icon.png";
	public static final String SPLASH_IMAGE = "/resources/Splash.jpg";
	public static final int SPLASH_WIDTH = 500;
	public static final int SPLASH_HEIGHT = 400;
	
	/*******************************************************************************
	 * BLOCK: Initialisation
	 *******************************************************************************/
	@Override
	public void init() {
		// Save this instance
		application = this;
		
		// Copy file to appropriate location if necessary
		autoUpdater = new AutoUpdater();
		autoUpdater.completeUpdates();
		
		// Load the essential stuff for the program
		initLocales();
		initFonts();
	}
	
	public void initLocales() {
		this.locale = new Locale("en");
		this.display = ResourceBundle.getBundle("resources.displayLanguage.DisplayBundle", locale);
	}
	
	public void initFonts() {
		try {
			Scanner in = new Scanner(getClass().getResourceAsStream("/resources/fonts/fonts.txt"));
			System.out.println("Loading fonts...");
			while (in.hasNextLine()) {
				String fontFile = in.nextLine();
				if (Font.loadFont(getClass().getResourceAsStream(fontFile), 10) != null)
					System.out.println("[SUCCESS] Loaded " + fontFile);
				else
					System.out.println("[FAILURE] Unable to load " + fontFile);
			}
			in.close();
		}
		catch (Exception ex) {
			System.out.println("[ERROR] Unable to load some fonts.");
			ex.printStackTrace();
		}
	}
	
	/*******************************************************************************
	 * BLOCK: Splash Screens
	 *******************************************************************************/
	private void showSplash(Stage initStage, SplashScreen splashScreen, Task<?> task, CompletionHandler onComplete) {
		splashScreen.loadProgress.progressProperty().bind(task.progressProperty());
		splashScreen.progressText.textProperty().bind(task.messageProperty());
		task.stateProperty().addListener((observableValue, oldState, newState) -> {
			if (newState == Worker.State.SUCCEEDED) {
				splashScreen.loadProgress.progressProperty().unbind();
				FadeTransition fadeSplashAnimation = new FadeTransition(Duration.seconds(1.4), splashScreen.splashLayout);
				fadeSplashAnimation.setFromValue(1.0);
				fadeSplashAnimation.setToValue(0.0);
				fadeSplashAnimation.setOnFinished(actionEvent -> initStage.close());
				fadeSplashAnimation.play();
				onComplete.execute();
			}
		});
		
	    Scene splashScene = new Scene(splashScreen.splashLayout);
	    initStage.initStyle(StageStyle.UNDECORATED);
	    final Rectangle2D bounds = Screen.getPrimary().getBounds();
	    initStage.setScene(splashScene);
	    initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
	    initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
	    initStage.setAlwaysOnTop(true);
	    initStage.show();
	}
	
	public interface CompletionHandler {
		public void execute();
	}
	
	/*******************************************************************************
	 * BLOCK: Program launching logic
	 *******************************************************************************/
	@Override
	public void start(Stage primaryStage) {
		try {			
			SplashScreen launchSplash = createLaunchSplash();
			showSplash(primaryStage, launchSplash, loadTask(), () -> showAuthenticationWindow());
			//toolManager = new ToolManager();
			//showDashboard();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public SplashScreen createLaunchSplash() {
		Label title = new Label("Lead! Tools");
		title.setFont(Font.font("Titillium Web", 48));
		title.setTextFill(Color.WHITE);
		Label subtitle = new Label("Powered by");
		subtitle.setFont(Font.font("Titillium Web", 32));
		subtitle.setTextFill(Color.WHITE);
		VBox textBox = new VBox(title, subtitle);
		textBox.setPadding(new Insets(20));
		
		StackPane.setAlignment(textBox, Pos.TOP_CENTER);
		ImageView splash = new ImageView(new Image(getClass().getResourceAsStream(SPLASH_IMAGE)));
		splash.setFitWidth(SPLASH_WIDTH);
		splash.setPreserveRatio(true);
		StackPane stack = new StackPane(splash, textBox);
		
		ProgressBar loadProgress = new ProgressBar();
		loadProgress.setPrefWidth(SPLASH_WIDTH);
		loadProgress.setStyle("-fx-accent: red");
		Label progressText = new Label("Loading application...");
		VBox splashLayout = new VBox();
		splashLayout.getChildren().addAll(stack, loadProgress, progressText);
		progressText.setAlignment(Pos.CENTER);
		splashLayout.setStyle("-fx-padding: 5; -fx-background-color: cornsilk; -fx-border-width:5; -fx-border-color: linear-gradient(to bottom, chocolate, derive(chocolate, 50%));");
		splashLayout.setEffect(new DropShadow());
		
		SplashScreen launchSplash = new SplashScreen(loadProgress, progressText, splashLayout);
		return launchSplash;
	}
	
	private Task<Void> loadTask() {
		Task<Void> task = new Task<Void>() {
			@Override public Void call() throws InterruptedException {
				this.updateProgress(0.0, 1.0);
				
				this.updateMessage("Reading user settings...");
				// TODO: Fill in after completing settings manager
				
				this.updateMessage("Checking for updates...");
				// TODO: Fill in after completing updater
				if (autoUpdater.hasUpdate()) {
					this.updateMessage("Update found. Your program may restart a few times.");
					autoUpdater.update();
				}
				
				this.updateMessage("Loading resources...");
				Thread.sleep(2000);
				this.updateProgress(0.5, 1.0);
				this.updateMessage("Getting everything ready...");
				Thread.sleep(2000);
				this.updateProgress(1.0, 1.0);
				Thread.sleep(200);
				this.updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 1.0);
				this.updateMessage("Done!");
				Thread.sleep(1200);
				return null;
			}
		};
		
		new Thread(task).start();;
		return task;
	}
	
	public void showAuthenticationWindow() {
		try {
			//Parent root = FXMLLoader.load(getClass().getResource("/views/Dashboard.fxml"));
			//Parent root = FXMLLoader.load(getClass().getResource("/views/Tool_Manage_Inventory.fxml"));
			//Parent root = FXMLLoader.load(getClass().getResource("/views/Tool_Manage_Manpower.fxml"));
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/Authentication.fxml"));
			Parent root = fxmlLoader.load();
			
			this.authenticator = fxmlLoader.getController();
			
			windowManager = new WindowManager(new Stage());
			windowManager.setSceneFromParent(root);
			
			windowManager.showPrimaryStage();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/*******************************************************************************
	 * BLOCK: Login/Not logging in + loading the actual program
	 *******************************************************************************/
	// This method is called from the authenticator
	public void continueWithoutLogin() {
		((Stage) this.windowManager.getCurrentScene().getWindow()).close();
		loadApplication();
	}
	
	// This method is called from the authenticator
	public void login(Task<Void> loginTask) {
		try {
			((Stage) this.windowManager.getCurrentScene().getWindow()).close();
			
			SplashScreen loginSplash = createLoadingSplash();	// reuse the splash screen design
			new Thread(loginTask).start();
			showSplash(new Stage(), loginSplash, loginTask, () -> loadApplication());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void loadApplication() {
		SplashScreen loadToolsSplash = createLoadingSplash();	// reuse the splash screen design
		this.toolManager = new ToolManager();
		Task<Void> loadToolsTask = toolManager.loadAllToolsTask();
		new Thread(loadToolsTask).start();
		showSplash(new Stage(), loadToolsSplash, loadToolsTask, () -> showDashboard());
	}
	
	public SplashScreen createLoadingSplash() {
		Label title = new Label(Main.getApplication().getDisplay().getString("loginSplashTitle"));
		title.setFont(Font.font("Titillium Web", 42));
		title.setTextFill(Color.WHITE);
		Label subtitle = new Label(Main.getApplication().getDisplay().getString("loginSplashContent"));
		subtitle.setFont(Font.font("Titillium Web", 32));
		subtitle.setTextFill(Color.WHITE);
		VBox textBox = new VBox(title, subtitle);
		textBox.setPadding(new Insets(20));
		
		StackPane.setAlignment(textBox, Pos.TOP_CENTER);
		ImageView splash = new ImageView(new Image(getClass().getResourceAsStream(SPLASH_IMAGE)));
		splash.setFitWidth(SPLASH_WIDTH);
		splash.setPreserveRatio(true);
		StackPane stack = new StackPane(splash, textBox);
		
		ProgressBar loadProgress = new ProgressBar();
		loadProgress.setPrefWidth(SPLASH_WIDTH);
		loadProgress.setStyle("-fx-accent: red");
		Label progressText = new Label("Loading application...");
		VBox splashLayout = new VBox();
		splashLayout.getChildren().addAll(stack, loadProgress, progressText);
		progressText.setAlignment(Pos.CENTER);
		splashLayout.setStyle("-fx-padding: 5; -fx-background-color: cornsilk; -fx-border-width:5; -fx-border-color: linear-gradient(to bottom, chocolate, derive(chocolate, 50%));");
		splashLayout.setEffect(new DropShadow());
		
		SplashScreen loginSplash = new SplashScreen(loadProgress, progressText, splashLayout);
		return loginSplash;
	}
	
	public void showDashboard() {
		try {
			Stage primaryStage = new Stage();
			windowManager = new WindowManager(primaryStage);
			
			Parent root = FXMLLoader.load(getClass().getResource("/views/Dashboard.fxml"), display);
			
			windowManager.setSceneFromParent(root);
			windowManager.showPrimaryStage();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/*******************************************************************************
	 * BLOCK: Helper functions
	 *******************************************************************************/
	public void logout() {
		this.authenticator = null;
		try {
			((Stage) this.windowManager.getCurrentScene().getWindow()).close();
			showAuthenticationWindow();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void logException(Exception ex) {
		// TODO Auto-generated method stub
		System.err.println("[CAUGHT EXCEPTION]");
		ex.printStackTrace();
		
		new File("data").mkdirs();
		File file = new File("data/error.log");
        PrintWriter out = null;
        try {
        	out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        	out.print(ex.toString());
        }
        catch (IOException ex2) {
        	Alert alert2 = new Alert(AlertType.ERROR);
			alert2.setTitle("Error");
	        alert2.setHeaderText("Unable to write to file");
	        String content2 = ex2.getMessage();
	        alert2.setContentText(content2);
	        alert2.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert2.show();
        }
        finally {
        	if (out != null) out.close();
        }
	}
	
	/*******************************************************************************
	 * BLOCK: Settings
	 *******************************************************************************/
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
		this.display = ResourceBundle.getBundle("resources.displayLanguage.DisplayBundle", locale);
	}

	public ResourceBundle getDisplay() {
		return display;
	}
	
	/*******************************************************************************
	 * BLOCK: Getters for the various managers
	 *******************************************************************************/
	public static Main getApplication() {
		return application;
	}
	
	public WindowManager getWindowManager() {
		return this.windowManager;
	}
	
	public ToolManager getToolManager() {
		return this.toolManager;
	}
	
	public AuthenticationController getAuthenticator() {
		return this.authenticator;
	}
	
	/*******************************************************************************
	 * BLOCK: For eclipse IDE to launch this properly
	 *******************************************************************************/
	public static void main(String[] args) {
		launch(args);
	}
}
