package models;

public class InvalidTemplateException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1284005550651257465L;

	public InvalidTemplateException() {
	}
	
	public InvalidTemplateException(Exception ex) {
		 super(ex);
	}
}

