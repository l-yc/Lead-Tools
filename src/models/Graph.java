package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Graph {
	protected int V;
	protected int E;
	protected List<List<Integer>> adjacencyList;
	
	protected Graph(int V) {
		this.V = V;
		this.adjacencyList = new ArrayList<List<Integer>>(V);
		for (int i = 0; i < V; ++i) this.adjacencyList.add(new ArrayList<Integer>());
	}
	
	protected Graph(int V, int E) {
		this.V = V;
		this.E = E;
		this.adjacencyList = new ArrayList<List<Integer>>(V);
		for (int i = 0; i < V; ++i) this.adjacencyList.add(new ArrayList<Integer>());
	}
	
	protected void shuffleAdjacencyList(int v) {
		Collections.shuffle(this.adjacencyList.get(v));
	}
	
	protected void shuffleAdjacencyListAll() {
		for (int v = 0; v < V; ++v)
			Collections.shuffle(this.adjacencyList.get(v));
	}
}
