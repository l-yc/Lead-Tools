package models;

public class UndirectedGraph extends Graph {
	public UndirectedGraph(int V) {
		super(V);
	}
	
	public UndirectedGraph(int V, int E) {
		super(V, E);
	}
	
	public void addEdge(int u, int v) {
		adjacencyList.get(u).add(v);
		adjacencyList.get(v).add(u);
	}
}
