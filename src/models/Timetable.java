package models;

import java.util.List;

public class Timetable extends TableData<Timeslot> {
//	public static final String ALGORITHM_ONE_TO_ONE = "1 Person/1 Slot";
//	public static final String ALGORITHM_ONE_TO_TWO = "1 Person/2 Slots";
//	public static final String ALGORITHM_TWO_TO_ONE = "2 Persons/1 Slot";
	public enum Algorithm {
		ONE_TO_ONE ("1 Slot/1 Person"),
		ONE_TO_TWO ("2 Slots/1 Person"),
		TWO_TO_ONE ("1 Slot/2 Persons");
		
		private final String description;
		
		private Algorithm(String description) {
			this.description = description;
		}
		
		@Override public String toString() {
			return this.description;
		}
	}
	
	public Timetable() {
		super();
	}
	
	// FOR DEBUGGING PURPOSES
//	public void populate() {
//		super.addRow("A");
//		super.addRow("B");
//		super.addRow("C");
//		super.addRow("D");
//		super.addRow("E");
//		super.addRow("F");
//		super.addRow("G");
//		super.addColumn("9 - 10");
//		super.addColumn("10 - 11");
//		super.addColumn("11 - 12");
//		
//		try {
//			super.populate(super.getRows(), super.getColumns(), new Timeslot());
//			super.setRowHeaders(super.rowHeaders);
//			super.setColumnHeaders(super.columnHeaders);
//		} catch (InvalidTemplateException e) {
//			e.printStackTrace();
//		}
//		
//		for (List<Timeslot> row : super.table) {
//			for (Timeslot slot : row) {
//				Random rand = new Random();
//				slot.setAvailable(rand.nextInt(2) == 0 ? false : true);
//				//slot.setAllocated(rand.nextInt(2) == 0 ? false : true);
//				System.out.print(slot.isAvailable());
//				System.out.print(", ");
//				System.out.println(slot.isAllocated());
//			}
//		}
//	}
	
	public TableData<Boolean> generateAllocations(Algorithm alg) {
		TableData<Boolean> allocations = new TableData<>();
		try {
			allocations.resize(super.getRows(), super.getColumns());
			allocations.populate(super.getRows(), super.getColumns(), new Boolean(false));
		} catch (InvalidTemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(allocations.getRows());
		System.out.println(allocations.getColumns());
		
		BipartiteGraph graph;
		int L, R;
		
		switch (alg) {
		case ONE_TO_ONE:
			graph = new BipartiteGraph(super.getRows(), super.getColumns());
			L = super.getRows();
			
			for (int i = 0; i < super.getRows(); ++i) {
				for (int j = 0; j < super.getColumns(); ++j) {
					if (super.getElement(i, j).isAvailable()) {
						graph.addEdge(i, super.getRows() + j);
					}
				}
			}
			break;
		case ONE_TO_TWO:
			graph = new BipartiteGraph(super.getRows()*2, super.getColumns());
			L = super.getRows()*2;
			
			for (int i = 0; i < super.getRows(); ++i) {
				for (int j = 0; j < super.getColumns(); ++j) {
					if (super.getElement(i, j).isAvailable()) {
						graph.addEdge(i, super.getRows()*2 + j);
						graph.addEdge(i + super.getRows(), super.getRows()*2 + j);
					}
				}
			}
			break;
		case TWO_TO_ONE:
			graph = new BipartiteGraph(super.getRows(), super.getColumns()*2);
			L = super.getRows();
			
			for (int i = 0; i < super.getRows(); ++i) {
				for (int j = 0; j < super.getColumns(); ++j) {
					if (super.getElement(i, j).isAvailable()) {
						graph.addEdge(i, super.getRows() + j);
						graph.addEdge(i, super.getRows() + j + super.getColumns());
					}
				}
			}
			break;
		default:
			return allocations;
		}
		
		List<Integer> matching = graph.getRandomMatching();
		
		for (int l = 0; l < L; ++l) {
			if (matching.get(l) != BipartiteGraph.UNMATCHED) {
				allocations.setElement(l % super.getRows(),
									   ((matching.get(l) - L) % super.getColumns()),
									   true);
				System.out.println(super.rowHeaders.get(l % super.getRows()) + " : " + super.columnHeaders.get((matching.get(l) - L) % super.getColumns()));
			}
		}
		
		return allocations;		
	}
}
