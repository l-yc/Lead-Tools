package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BipartiteGraph extends UndirectedGraph {
	public static final int UNMATCHED = -1;
	public static final int MATCHED = 1;
	public static final int UNVISITED = 0;
	public static final int VISITED = 1;
	protected int L;
	protected int R;
	protected List<Integer> matching;
	protected List<Integer> visited;
	
	public BipartiteGraph(int L, int R) {
		super(L + R);
		this.L = L;
		this.R = R;
	}
	
	public BipartiteGraph(int L, int R, int E) {
		super(L + R, E);
		this.L = L;
		this.R = R;
	}
	
	private int match(int l) {
		if (this.visited.get(l) == VISITED) return UNMATCHED;
		this.visited.set(l, 1);
		for (int r : super.adjacencyList.get(l)) {
			int matchR = this.matching.get(r);
			if (matchR == UNMATCHED || match(matchR) == MATCHED) {
				this.matching.set(r, l);
				this.matching.set(l, r);
				return MATCHED;
			}
		}
		return UNMATCHED;
	}
	
	public List<Integer> getRandomMatching() {
		this.matching = new ArrayList<>(V);
		this.visited = new ArrayList<>(V);
		
		for (int v = 0; v < V; ++v) this.matching.add(UNMATCHED);
		for (int v = 0; v < V; ++v) this.visited.add(UNVISITED);
		
		super.shuffleAdjacencyListAll();
		ArrayList<Integer> lList = new ArrayList<Integer>();
		for (int l = 0; l < this.L; ++l) lList.add(l);
		Collections.shuffle(lList);
		
		for (int i = 0; i < this.L; ++i) {
			int l = lList.get(i);
			if (this.matching.get(l) == UNMATCHED) {
				for (int v = 0; v < V; ++v) 
					this.visited.set(v, UNVISITED);
				match(l);
			}
		}
		
		return this.matching;
	}
}
