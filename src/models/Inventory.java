package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Inventory {
	public class Item {
		private StringProperty name;
		private IntegerProperty quantity;
		private IntegerProperty loaned;
		
		public Item(String name, int quantity) {
			this(name, quantity, 0);
		}
		
		public Item(String name, int quantity, int loaned) {
			this.name = new SimpleStringProperty(name);
			this.quantity = new SimpleIntegerProperty(quantity);
			this.loaned = new SimpleIntegerProperty(loaned);
		}

		public String getName() {
			return name.get();
		}

		public void setName(String name) {
			this.name.set(name);
		}
		
		public StringProperty nameProperty() {
			return this.name;
		}

		public int getQuantity() {
			return quantity.get();
		}

		public void setQuantity(int quantity) {
			this.quantity.set(quantity);
		}
		
		public IntegerProperty quantityProperty() {
			return this.quantity;
		}
		
		public int getLoaned() {
			return loaned.get();
		}
		
		public void setLoaned(int loaned) {
			this.loaned.set(loaned);
		}
		
		public IntegerProperty loanedProperty() {
			return this.loaned;
		}
		
		public String toString() {
			return String.format("%s - %d", this.name.get(), this.quantity.get());
		}
		
		public boolean loanItem() {
			if (this.getQuantity() > 0) {
				this.setQuantity(this.getQuantity()-1);
				this.setLoaned(this.getLoaned()+1);
				return true;
			}
			else return false;
		}
		
		public boolean returnItem() {
			if (this.getLoaned() > 0) {
				this.setLoaned(this.getLoaned()-1);
				this.setQuantity(this.getQuantity()+1);
				return true;
			}
			else return false;
		}
	}
	
	private List<Item> inventoryList;
	public Inventory() {
		this.inventoryList = new ArrayList<Item>();
	}
	
//	// dummy stuff for testing
//	public void populate() {
//		this.inventoryList.add(new Item("Light sensors", 5));
//		this.inventoryList.add(new Item("Sound sensors", 15));
//		this.inventoryList.add(new Item("Medium motors", 10));
//	}
	
	public void clear() {
		this.inventoryList.clear();
	}
	
	public void removeItem(Item item) {
		this.inventoryList.remove(item);
	}
	
	public void addItem(Item item) {
		this.inventoryList.add(item);
	}
	
	public List<Item> getInventoryList() {
		return Collections.unmodifiableList(this.inventoryList);
	}
	
	public void sort() {
		Collections.sort(this.inventoryList, (Item a, Item b) -> {
			return a.getName().compareTo(b.getName());
		});
	}
}
