package models;

public class Timeslot {
	private boolean isAvailable;
	private boolean isAllocated;
	
	public Timeslot() {
		this(false, false);
	}
	
	public Timeslot(boolean isAvailable, boolean isAllocated) {
		this.isAvailable = isAvailable;
		this.isAllocated = isAllocated;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public boolean isAllocated() {
		return isAllocated;
	}

	public void setAllocated(boolean isAllocated) {
		this.isAllocated = isAllocated;
	}
}