package models;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TableData<T> {
	List<List<T>> table;
	List<String> rowHeaders;
	List<String> columnHeaders;
	private int rows;
	private int columns;
	
	public TableData() {
		this.table = new ArrayList<List<T>>();
		this.rowHeaders = new ArrayList<String>();
		this.columnHeaders = new ArrayList<String>();
	}
	
	public void resize(int row, int col) {
		while (this.rows != row) {
			if (this.rows > row) this.removeRow(this.rows-1);
			else this.addRow();
		}
		while (this.columns != col) {
			if (this.columns > col) this.removeColumn(this.columns-1);
			else this.addColumn();
		}
	}
	
	public void populate(int startRow, int startCol, int endRow, int endCol, T template) throws InvalidTemplateException {
		for (int i = startRow; i < endRow; ++i) {
			for (int j = startCol; j < endCol; ++j) {
				try {
					T clone;
					if (template instanceof Boolean)
						clone = (T) new Boolean(false);
					else if (template instanceof Integer)
						clone = (T) new Integer(0);
					else {
						clone = (T) template.getClass().newInstance();
						
						for (Field f : template.getClass().getFields()) {
							f.set(clone, template.getClass().getField(f.getName()).get(template));
						}
					}
					this.table.get(i).set(j, clone);
				}
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
					throw new InvalidTemplateException(ex);
				}
			}
		}
	}
	
	public void populate(int row, int col, T template) throws InvalidTemplateException {
		populate(0, 0, row, col, template);
	}
	
	public List<T> getRow(int idx) {
		return this.table.get(idx);
	}
	
	public List<T> getColumn(int idx) {
		List<T> col = new ArrayList<T>();
		for (List<T> row : this.table) {
			col.add(row.get(idx));
		}
		return col;
	}
	
	public void addRow() {
		List<T> e = new ArrayList<T>();
		for (int i = 0; i < this.columns; ++i) e.add(null);
		addRow(this.rows, e, null);
	}
	
	public void addRow(String rowHeader) {
		addRow(this.rows, rowHeader);
	}
	
	public void addRow(int idx, String rowHeader) {
		List<T> e = new ArrayList<T>();
		for (int i = 0; i < this.columns; ++i) e.add(null);
		addRow(idx, e, rowHeader);
	}
	
	public void addRow(List<T> e) {
		addRow(this.rows, e, null);
	}
	
	public void addRow(int idx, List<T> e) {
		addRow(idx, e, null);
	}
	
	public void addRow(int idx, List<T> e, String rowHeader) {
		this.table.add(idx, e);
		this.rowHeaders.add(idx, rowHeader);
		++this.rows;
	}
	
	public void addColumn() {
		List<T> e = new ArrayList<T>();
		for (int i = 0; i < this.rows; ++i) e.add(null);
		addColumn(this.columns, e, null);
	}
	
	public void addColumn(String columnHeader) {
		addColumn(this.columns, columnHeader);
	}
	
	public void addColumn(int idx, String columnHeader) {
		List<T> e = new ArrayList<T>();
		for (int i = 0; i < this.rows; ++i) e.add(null);
		addColumn(idx, e, columnHeader);
	}
	
	public void addColumn(List<T> e) {
		addColumn(this.columns, e, null);
	}
	
	public void addColumn(int idx, List<T> e) {
		addColumn(idx, e, null);
	}
	
	public void addColumn(int idx, List<T> e, String columnHeader) {
		for (int row = 0; row < this.rows; ++row) {
			this.table.get(row).add(idx, e.get(row));
		}
		this.columnHeaders.add(columnHeader);
		++this.columns;
	}
	
	public void removeRow(int idx) {
		this.table.remove(idx);
		this.rowHeaders.remove(idx);
	}
	
	public void removeColumn(int idx) {
		for (int row = 0; row < this.rows; ++row) {
			this.table.get(row).remove(idx);
		}
		this.columnHeaders.remove(idx);
	}
	
	public void setRow(int idx, List<T> e) {
		this.table.set(idx, e);
	}
	
	public void setColumn(int idx, List<T> e) {
		for (int row = 0; row < this.table.size(); ++row) {
			this.table.get(row).set(idx, e.get(row));
		}
	}
	
	public T getElement(int row, int col) {
		return this.table.get(row).get(col);
	}
	
	public void setElement(int row, int col, T e) {
		this.table.get(row).set(col, e);
	}
	
	public int getRows() {
		return this.rows;
	}
	
	public int getColumns() {
		return this.columns;
	}
	
	// Read-only
	public List<String> getRowHeaders() {
		return Collections.unmodifiableList(this.rowHeaders);
	}
	
	// Read-only
	public List<String> getColumnHeaders() {
		return Collections.unmodifiableList(this.columnHeaders);
	}
	
	public void addRowHeaders(List<String> headers) {
		for (String header : headers) {
			this.addRow(header);
		}
	}
	
	public void addColumnHeaders(List<String> headers) {
		for (String header : headers) {
			this.addColumn(header);
		}
	}
	
	public void setRowHeaders(List<String> headers) {
		this.resize(headers.size(), this.columns);
		this.rowHeaders = headers;
	}
	
	public void setColumnHeaders(List<String> headers) {
		this.resize(this.rows, headers.size());
		this.columnHeaders = headers;
	}
	
	public void setRowHeader(int idx, String rowHeader) {
		this.rowHeaders.set(idx, rowHeader);
	}
	
	public void setColumnHeader(int idx, String columnHeader) {
		this.columnHeaders.set(idx, columnHeader);
	}
	
	public List<List<Object>> asList() {
		List<List<Object>> list = new ArrayList<>();
		
		List<Object> colHeader = new ArrayList<>(this.columnHeaders);
		colHeader.add(0, "");
		
		list.add(colHeader);
		
		for (int i = 0; i < this.table.size(); ++i) {
			List<Object> newRow = new ArrayList<>(this.table.get(i));
			newRow.add(0, this.rowHeaders.get(i));
			list.add(newRow);
		}
		
		return list;
	}
}
