package views;

import javafx.scene.Node;
import javafx.scene.layout.*;

public class Widget extends AnchorPane {
	public Widget(Node n) {
		super.getChildren().add(n);
		setTopAnchor(n, 0.0);
		setRightAnchor(n, 0.0);
		setBottomAnchor(n, 0.0);
		setLeftAnchor(n, 0.0);
		this.getStyleClass().add("widget");
	}
}
