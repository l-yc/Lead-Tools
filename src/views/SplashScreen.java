package views;

import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;

public class SplashScreen {
	public final ProgressBar loadProgress;
	public final Label progressText;
	public final VBox splashLayout;
	
	public SplashScreen(ProgressBar loadProgress, Label progressText, VBox splashLayout) {
		this.loadProgress = loadProgress;
		this.progressText = progressText;
		this.splashLayout = splashLayout;
	}
	
	public ProgressBar getLoadProgress() {
		return loadProgress;
	}
	public Label getProgressText() {
		return progressText;
	}
	public VBox getSplashLayout() {
		return splashLayout;
	}

	
}
