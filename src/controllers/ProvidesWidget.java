package controllers;

import views.Widget;

public interface ProvidesWidget {
	public Widget exportWidget();
}
