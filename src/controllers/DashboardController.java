package controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;
import controllers.core.ToolManager;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import views.Widget;

public class DashboardController extends AuthenticatedController {
	@FXML private Label welcomeMessage;
	@FXML private Label profileName;
	private ToolManager toolManager;
	@FXML private Accordion toolBox;
	@FXML private FlowPane widgetPane;
	@FXML Button logoutBtn;
	
	public DashboardController() {
		toolManager = Main.getApplication().getToolManager();
	}
	
	@FXML public void initialize() {
		// Make the user feel at home
		initializeUserProfile();
		
		// Set up toolBox
//		Task<Void> task = toolManager.loadAllToolsTask();
//		Thread t = new Thread(task);
//		t.start();
//		try {
//			task.get();
//		} catch (InterruptedException | ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		// delete the above code after finalising
		initializeToolBox();
		
		// Load widgets
		initializeWidgets();
	}
	
	/** 
	 * Set up toolBox with toolManager
	 */
	public void initializeUserProfile() {
		String givenName = "", displayName = "";
		if (Main.getApplication().getAuthenticator().isLoggedIn()) {
			// Query for user profile name
			Content content = null;
			try {
				content = Request.Get("https://graph.microsoft.com/v1.0/me/")
						.addHeader("Authorization", Main.getApplication().getAuthenticator().getAccessToken())
					    .execute().returnContent();
			} catch (HttpResponseException e) {
				// TODO Auto-generated catch block
				System.out.println("[ERROR] Could not login");
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				System.out.println("[ERROR] Disconnected");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Response: " + content);
			if (content != null) {
				JsonObject json = new JsonParser().parse(content.asString()).getAsJsonObject();
				givenName = json.get("givenName").getAsString();
				displayName = json.get("displayName").getAsString();
			}
		}
		else {
			givenName = Main.getApplication().getDisplay().getString("defaultUsername");
			displayName = givenName;
		}
		
		welcomeMessage.setText(Main.getApplication().getDisplay().getString("welcome") + givenName);
		profileName.setText(Main.getApplication().getDisplay().getString("loggedIn") + "\r\n" + displayName);
	}
	
	/** 
	 * Set up toolBox with toolManager
	 */
	public void initializeToolBox() {
		for (String category : this.toolManager.getToolCategories()) {
			System.out.println("category: " + category);
			VBox vbox = new VBox();
			for (String name : this.toolManager.getToolsByCategory(category)) {
				Label label = new Label(name);
				label.setPrefWidth(Double.POSITIVE_INFINITY);
				label.setFont(Font.font(18));
				label.setPadding(new Insets(15, 30, 15, 30));
				System.out.println("\tname: " + name);
				label.setOnMouseClicked(e -> {
					//Parent parent = this.toolManager.loadTool(category, name);
					Scene scene = this.toolManager.loadToolScene(category, name);
					if (scene != null) {
						//Main.getApplication().getWindowManager().setSceneFromParent(parent);
						Main.getApplication().getWindowManager().setTitle(category + " - " + name);
						Main.getApplication().getWindowManager().setScene(scene);
					}
				});
				
				label.getStyleClass().add("vboxListItem");
				vbox.getChildren().add(label);
			}			
			
			AnchorPane anchorPane = new AnchorPane(vbox);
			anchorPane.setPadding(new Insets(0));
			AnchorPane.setTopAnchor((Node)vbox, 0.0);
			AnchorPane.setRightAnchor((Node)vbox, 0.0);
			AnchorPane.setBottomAnchor((Node)vbox, 0.0);
			AnchorPane.setLeftAnchor((Node)vbox, 0.0);
			
			TitledPane titledPane = new TitledPane(category, anchorPane);
			titledPane.setFont(Font.font(20));
			this.toolBox.getPanes().add(titledPane);
		}
	}
	
	/** 
	 * Set up widgets for available tools
	 */
	public void initializeWidgets() {
		for (Widget w : this.toolManager.getWidgets()) {
			if (w != null) {
				this.widgetPane.getChildren().add(w);
				FlowPane.setMargin(w, new Insets(10));
			}
		}
	}
	
	/**
	 * Code for toolbar buttons
	 */
	@FXML public void showAbout() {
		Label title = new Label("Created by lyc");
		Label links = new Label("Links: ");
		Hyperlink github = new Hyperlink("Github");
		github.setOnAction(e -> {
			Main.getApplication().getHostServices().showDocument("https://github.com/l-yc");
		});
		
		HBox hbox = new HBox(links, github);
		VBox vbox = new VBox(10);
		
		vbox.getChildren().addAll(title, hbox);
		vbox.setStyle("-fx-font-size: 24");
		vbox.setPadding(new Insets(10));
		Scene scene = new Scene(vbox);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	
	@FXML public void changeDisplayLanguage() {
		List<String> choices = Arrays.asList("English", "\u7B80\u4F53\u4E2D\u6587");
		ChoiceDialog<String> cd = new ChoiceDialog<String>(choices.get(0), choices);
		cd.setTitle(Main.getApplication().getDisplay().getString("selectDisplayLanguageTitle"));
		cd.setHeaderText(Main.getApplication().getDisplay().getString("selectDisplayLanguageHeader"));
		cd.setContentText(Main.getApplication().getDisplay().getString("selectDisplayLanguageContent"));
		cd.showAndWait();
		
		String item = cd.getResult();
		if (item == null) return;
		
		Locale locale = null;
		switch (item) {
		case "English":
			locale = new Locale("en");
			break;
		case "\u7B80\u4F53\u4E2D\u6587":
			locale = new Locale("zh");
			break;
		}
		
		if (locale.equals(Main.getApplication().getLocale())) return;
		Main.getApplication().setLocale(locale);
		Main.getApplication().getWindowManager().getPrimaryStage().close();
		Main.getApplication().loadApplication();
	}
	
}