package controllers.core;

import java.util.Stack;

import application.Main;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class WindowManager {
	private Stage primaryStage;
	private Stack<Scene> sceneStack;
	private boolean isFullScreen;
	
	public WindowManager(Stage primaryStage) {
		this.sceneStack = new Stack<Scene>();
		
		this.primaryStage = primaryStage;
		primaryStage.getIcons().add(new Image(
				getClass().getResourceAsStream(Main.APPLICATION_ICON)
		));
		this.primaryStage.setTitle("Lead! Tools");
		
		this.primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
            	primaryStage.setWidth((double) newVal); 
            }
        });
        this.primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
            	primaryStage.setHeight((double) newVal);
            }
        });
		
		this.primaryStage.setOnCloseRequest(e -> {
			Platform.exit();
			System.exit(0);
		});
		
		this.isFullScreen = false;
		this.primaryStage.setFullScreenExitHint("");
		this.primaryStage.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
			if (event.getCode().equals(KeyCode.F11)) {
				setFullScreen(true);
				event.consume();
			}
		});
	}
	
	public boolean isFullScreen() {
		return this.isFullScreen;
	}
	
	public void toggleFullScreen() {
		this.isFullScreen = !this.isFullScreen;
		this.primaryStage.setFullScreen(this.isFullScreen);
	}
	
	public void setFullScreen(boolean flag) {
		this.isFullScreen = flag;
		this.primaryStage.setFullScreen(flag);
	}
	
	public void setScene(Scene scene) {
		sceneStack.push(scene);
		this.primaryStage.setScene(sceneStack.peek());
		setFullScreen(isFullScreen());
	}
	
	public void revertScene() {
		sceneStack.pop();
		setScene(sceneStack.peek());
	}
	
	public Scene makeSceneFromParent(Parent parent, String stylesheet) {
		Scene scene = new Scene(parent);
		scene.getStylesheets().add(getClass().getResource(stylesheet).toExternalForm());
		return scene;
	}
	
	public Scene makeSceneFromParent(Parent parent) {
		return makeSceneFromParent(parent, "/views/application.css");
	}
	
	public void setSceneFromParent(Parent parent, String stylesheet) {
		Scene scene = makeSceneFromParent(parent, stylesheet);
		this.setScene(scene);
	}
	
	public void setSceneFromParent(Parent parent) {
		this.setSceneFromParent(parent, "/views/application.css");
	}
	
	public Scene getCurrentScene() {
		return sceneStack.peek();
	}
	
	public Stage getPrimaryStage() {
		return this.primaryStage;
	}
	
	public void hidePrimaryStage() {
		this.primaryStage.hide();
	}
	
	public void showPrimaryStage() {
		this.primaryStage.setMaximized(true);
		this.primaryStage.show();
		this.primaryStage.requestFocus();
	}

	public void setTitle(String string) {
		// TODO Auto-generated method stub
		if (string == null)
			this.primaryStage.setTitle(Main.APPLICATION_NAME);
		else
			this.primaryStage.setTitle(Main.APPLICATION_NAME + " - " + string);
	}
}
