package controllers.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;

public class SettingsManager {
	private static final String STORAGE_DIRECTORY = "data";
	private static final String APP_SETTINGS_FILEPATH = "data/settings.json";
	
	private JsonObject appSettings = null;
	
	public SettingsManager() {
		// Make sure directory exists
		new File(STORAGE_DIRECTORY).mkdirs();
	}
	
	public void readSettings() {
		FileInputStream fin = null;
		BufferedInputStream in = null;
		try {
			fin = new FileInputStream(APP_SETTINGS_FILEPATH);
			in = new BufferedInputStream(fin);
			byte[] data = new byte[fin.available()];
			for (int i = 0, curByte = in.read(); curByte != -1; i++, curByte = in.read()) {
				data[i] = (byte) curByte;
			}
			
			String decoded = new String(data, "UTF-8");
			System.out.println("Decoded: " + decoded);
			
			this.appSettings = new JsonParser().parse(decoded).getAsJsonObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Main.logException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Main.logException(e);
		}
		finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void writeSettings() {
		FileOutputStream fout = null;
		BufferedOutputStream out = null;
		try {
			fout = new FileOutputStream(APP_SETTINGS_FILEPATH);
			out = new BufferedOutputStream(fout);
			byte[] data = this.appSettings.toString().getBytes();
			out.write(data);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Main.logException(e);
		}
		finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void updateSetting(String key, String value) {
		this.appSettings.addProperty(key, value);
	}
	
	public static void main(String[] args) {
		SettingsManager sm = new SettingsManager();
		sm.readSettings();
		System.out.println(sm.appSettings);
		sm.updateSetting("name", "Lead! Tools");
		sm.writeSettings();
		System.out.println(sm.appSettings);
	}
}
