package controllers.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.impl.cookie.RFC2109DomainHandler;
import org.apache.http.util.EntityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;

public class AutoUpdater {
	private static final double APP_VERSION = 1.0;
	private static final String INFORMATION_URL = "http://parangninja.sytes.net/projects/Lead!Tools/info";
	private static final String DOWNLOAD_URL = "http://parangninja.sytes.net/projects/Lead!Tools/latestVersion";
	private static final String INFORMATION_FILEPATH = "data/info.json";
	private static final String DOWNLOAD_DIRPATH = "data/";
	private static final String DOWNLOAD_FILEPATH = DOWNLOAD_DIRPATH + "Lead!Tools.jar";
	private static final String MARKER_STRING = "marker.tmp";
	private static final String MARKER_FILEPATH = "data/" + MARKER_STRING;
	private static final String UPDATE_DIRPATH = "../";
	private static final String UPDATE_FILEPATH = UPDATE_DIRPATH + "Lead!Tools.jar";
	
	public AutoUpdater() {}
	
	private void downloadUpdate() {
		(new File("data")).mkdir();
		try {
			Request request;
			request = Request.Get(INFORMATION_URL);
			System.out.println("Retrieving app info...");
			request.execute().saveContent(new File(INFORMATION_FILEPATH));
			
			request = Request.Get(DOWNLOAD_URL);
			System.out.println("Retrieving latest version...");
			request.execute().saveContent(new File(DOWNLOAD_FILEPATH));
			File tmp = new File(MARKER_FILEPATH);
			tmp.createNewFile();
			
			System.out.println("Download complete!");
		}
		catch (IOException ex) {
			Main.logException(ex);
		}
	}

	public double getLatestVersion() {
		double version = -1.0;
		try {
			Request request = Request.Get(INFORMATION_URL);
			System.out.println("Retrieving version info...");
			
			HttpResponse response = request.execute().returnResponse();
			System.out.println(response.getStatusLine());
			
			String content = EntityUtils.toString(response.getEntity());
			System.out.println(content);
			
			JsonObject json = new JsonParser().parse(content).getAsJsonObject();
			version = json.get("version").getAsDouble();
		}
		catch (IOException ex) {
			Main.logException(ex);
		}
		return version;
	}
	
	public boolean hasUpdate() {
		double latestVersion = this.getLatestVersion();
		return latestVersion > APP_VERSION;
	}
	
	public void update() {
		this.downloadUpdate();
		
		try {
			System.out.println("Relaunching...");
			
			File f = new File(DOWNLOAD_FILEPATH);
			
			String[] cmd = {"java", "-jar", f.getAbsolutePath()};
			String[] envp = null;
			
			Process process = Runtime.getRuntime().exec(cmd, envp, new File(DOWNLOAD_DIRPATH));
			Thread.sleep(250);
			try {
				int exitValue = process.exitValue();	// throws error if still running
				System.out.println("Exit value: " + exitValue);
				if (exitValue == 0) throw new IllegalThreadStateException();	// if exited with no error
				
				byte[] b = new byte[process.getInputStream().available()];
				process.getInputStream().read(b);
				System.out.println(new String(b, "UTF-8"));
				byte[] err = new byte[process.getErrorStream().available()];
				process.getErrorStream().read(err);
				System.out.println(new String(err, "UTF-8"));
				System.out.println("Failed to launch.");
				
				// TODO: Add some code to alert user
			}
			catch (IllegalThreadStateException ex) {
				System.out.println("Launched successfully.");
				// code below useless, find a way to print output to screen
//				byte[] b = new byte[process.getInputStream().available()];
//				process.getInputStream().read(b);
//				System.out.println(new String(b, "UTF-8"));
				System.exit(0);
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void completeUpdates() {
		System.out.println("Completing Updates");	
		if (new File(MARKER_STRING).isFile()) {
			File source = null;
			String sourceFilePath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
			// Decode the sourceFilePath, which is encoded as an URL, to avoid spaces messing up
			try {
				source = new File(URLDecoder.decode(sourceFilePath, StandardCharsets.UTF_8.name()));
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			File target = new File(UPDATE_FILEPATH);
			try {
				Files.copy(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Copied " + source.getAbsolutePath() + " to " + target.getAbsolutePath() + " successfully.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (target.isFile()) {
				String[] cmd = {"java", "-jar", target.getAbsolutePath()};
				String[] envp = null;
				
				try {
					Process process = Runtime.getRuntime().exec(cmd, envp, new File(UPDATE_DIRPATH));
					System.out.println("Launched updated program.");
					System.out.println("Update completed successfully.");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.exit(0);
		}
		else if (new File(MARKER_FILEPATH).isFile()) {
			System.out.println("This is the new launch");	// doesn't show up either
			new File(MARKER_FILEPATH).delete();
			new File(DOWNLOAD_FILEPATH).delete();
		}
	}
	
	/**
	 * MARK: Driver for testing
	 */
	public static void main(String[] args) {
		AutoUpdater au = new AutoUpdater();
		//System.out.println(au.getLatestVersion());
		//au.downloadUpdate();
		//au.completeUpdates();
	}
}
