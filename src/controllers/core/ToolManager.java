package controllers.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.Main;
import controllers.Tool;
import controllers.ToolController;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Region;
import javafx.util.Pair;
import views.Widget;

public class ToolManager {
	List<String> categories;
	Map<String, List<String> > toolsByCategory;
	Map<Pair<String, String>, Scene> loadedTools;
	List<Widget> loadedWidgets;
	
	public ToolManager() {
		this.categories = Arrays.asList("Schedule", "Manage", "Miscellaneous");
		this.toolsByCategory = new HashMap<String, List<String> >();
		toolsByCategory.put("Schedule", Arrays.asList("Tasks", "Calendar", "Meeting"));
		toolsByCategory.put("Manage", Arrays.asList("Inventory", "Manpower"));
		toolsByCategory.put("Miscellaneous", Arrays.asList());
		this.loadedTools = new HashMap<Pair<String, String>, Scene>();
		this.loadedWidgets = new ArrayList<Widget>();
	}
	
	public List<String> getToolCategories() {
		 return Collections.unmodifiableList(this.categories);
	}
	
	public List<String> getToolsByCategory(String category) {
		return Collections.unmodifiableList(this.toolsByCategory.get(category));
	}
	
	public Task<Void> loadAllToolsTask() {
		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {
				for (Map.Entry<String, List<String> > entry : toolsByCategory.entrySet()) {
				    String category = entry.getKey();
				    List<String> names = entry.getValue();
				    for (int i = 0; i < names.size(); ++i) {
				    	String name = names.get(i);
				    	
				    	Tool tool = loadToolDynamically(category, name);
				    	System.out.println("Fetching (" + category + ") " + name + "...");
				    	updateMessage("Fetching (" + category + ") " + name + "...");
				    	updateProgress(i*2, names.size()*2);
		
				    	try {
				    		Scene scene = null;;
					    	Widget widget = null;
					    	if (tool.getParent() != null) {
					    		scene = Main.getApplication().getWindowManager().makeSceneFromParent(tool.getParent());
					    		widget = tool.getController().getWidget();
					    	}
					    	loadedTools.put(new Pair<>(category, name), scene);
					    	loadedWidgets.add(widget);
					    	
					    	System.out.println("Added " + tool);
					    	updateMessage("Added " + tool);
					    	updateProgress(i*2 + 1, names.size()*2);
				    	} catch (Exception ex) {
				    		ex.printStackTrace();
				    	}
				    }
				}
				return null;
			}
		};
		return task;
	}
	
	public Scene loadToolScene(String category, String name) {
		Scene scene = this.loadedTools.get(new Pair<>(category, name));
		if (scene == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Oops!");
	        alert.setHeaderText("This tool is in development or was not loaded successfully.");
	        String content = "We're sorry that it isn't available at the moment, but do check back in the future!";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
	        return null;
		}
		return scene;
	}
	
	public Tool loadToolDynamically(String category, String name) {
		// TODO Auto-generated method stub
		System.out.println("Loading tool...(" + category + ", " + name + ")");
		Parent root = null;
		ToolController controller = null;
		File dataFile = null;
		try {
			String view = String.format("/views/Tool_%s_%s.fxml", category, name);
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(view));
			root = fxmlLoader.load();
			
			controller = fxmlLoader.getController();
			
			new File("data").mkdirs();
			String data = String.format("data/Tool_%s_%s.data", category, name);
			dataFile = new File(data);
		}
		catch (NullPointerException | IllegalStateException ex) {
			System.out.printf("[ERROR]: (%s - %s) is unavailable.", category, name);
			System.out.println();
		}
		catch (IOException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid Operation");
	        alert.setHeaderText("There was a problem accessing the requested tool: (" + category + ") " + name);
	        String content = "The error has been recorded into a log file. "
	        		+ "Please contact the developer with the message.";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
	        
	        Main.logException(ex);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		Tool tool = new Tool(category, name, root, controller, dataFile);
		if (controller != null) controller.setTool(tool);
		return tool;
	}
	
	public List<Widget> getWidgets() {
		return Collections.unmodifiableList(this.loadedWidgets);
	}
}
