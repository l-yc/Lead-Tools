package controllers;

import application.Main;
import javafx.fxml.FXML;
import views.Widget;

public abstract class ToolController extends AuthenticatedController {
	private Tool tool;
	
	@FXML public void quitTool() {
		Main.getApplication().getWindowManager().setTitle(null);
		Main.getApplication().getWindowManager().revertScene();
	}
	
	public void setTool(Tool tool) {
		this.tool = tool;
	}
	
	public Tool getTool() {
		return this.tool;
	}
	
	// implement code to return a nice graph
	public abstract Widget getWidget();
}
