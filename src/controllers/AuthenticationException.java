package controllers;

public class AuthenticationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4713616807957526967L;

	public AuthenticationException() {
	}
	
	public AuthenticationException(Exception ex) {
		 super(ex);
	}
}

