package controllers;

public class InvalidDocumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2348534545920183826L;

	public InvalidDocumentException() {
	}
	
	public InvalidDocumentException(Exception ex) {
		 super(ex);
	}
}
