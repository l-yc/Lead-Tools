package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import application.Main;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import models.Inventory;
import views.Widget;

public class Tool_Manage_InventoryController extends ToolController {
	@FXML private Button backBtn;
	@FXML private Button logoutBtn;
	@FXML private VBox inventoryListWrapper;
	private VBox inventoryList;
	@FXML private Inventory inventory;
	@FXML private Button loadBtn;
	@FXML private Button saveBtn;
	@FXML private Button saveAsBtn;
	@FXML private Button sort;
	private File loadedFile = null;
	@FXML private Label loadedFileDisplay;
	private TextField searchBar;
	@FXML TitledPane manageLoansPane;
	
	@FXML ProgressBar progress;
	@FXML Label leftStatus;
	@FXML Label RightStatus;
	
	Inventory.Item selectedItem;
	HBox selectedItemHBox;
	@FXML Label itemName;
	@FXML Label itemQuantity;
	@FXML Label itemLoaned;
	
	private Timer searchBarTimer = new Timer();
	
	private long startTime, endTime;
	
	public Tool_Manage_InventoryController() {
		inventory = new Inventory();
	}
	
	@FXML public void initialize() {
		this.startTime = System.nanoTime();
		
		initInventoryListDisplay();
		initLoanMenu();
	}
	
	public void initInventoryListDisplay() {
		inventoryListWrapper.setSpacing(10);
		inventoryListWrapper.setCache(true);
		
		searchBar = new TextField();
		searchBar.setPromptText("Type to search...");
		searchBar.setFont(Font.font(20));
		progress.setStyle("-fx-accent: red");
		progress.setVisible(false);
		
		searchBar.textProperty().addListener((observableValue, oldValue, newValue) -> {
			final TimerTask searchTask = new TimerTask() {
				@Override
				public void run() {
					Platform.runLater(() -> inventoryList.getChildren().clear());
					for (Inventory.Item item : inventory.getInventoryList()) {
						String regex = ".*" + newValue + ".*";
						if (item.toString().matches(regex)) {
							Platform.runLater(() -> inventoryList.getChildren().add(createItem(item)));
						}
					}
					
					progress.setVisible(false);
				}
			};
			searchBarTimer.cancel();
			searchBarTimer = new Timer();
			searchBarTimer.schedule(searchTask, 500);
			progress.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
			progress.setVisible(true);
		});
		
		Label itemHeader = new Label("Item");
		itemHeader.setFont(Font.font(24));
		itemHeader.setPadding(new Insets(10));
		itemHeader.setPrefWidth(500);
		Label quantityHeader = new Label("Quantity");
		quantityHeader.setFont(Font.font(24));
		quantityHeader.setPadding(new Insets(10));
		quantityHeader.setPrefWidth(500);
		HBox header = new HBox(10, itemHeader, quantityHeader);
		
		inventoryList = new VBox(10);
		inventoryList.setCache(true);
		
		this.inventoryListWrapper.getChildren().add(searchBar);
		this.inventoryListWrapper.getChildren().add(header);
		this.inventoryListWrapper.getChildren().add(this.inventoryList);
		
		TextField name = new TextField("Name");
		name.setPadding(new Insets(10));
		name.setPrefWidth(500);
		name.setFont(Font.font(24));
		name.setDisable(true);
		name.setBackground(null);
		
		TextField quantity = new TextField("Quantity");
		quantity.setPrefWidth(500);
		quantity.setPadding(new Insets(10));
		quantity.setFont(Font.font(24));
		quantity.setDisable(true);
		quantity.setBackground(null);
		
		HBox hbox = new HBox(10, name, quantity);
		hbox.getStyleClass().add("listItem");
		//hbox.setDisable(true);
		this.inventoryListWrapper.getChildren().addAll(hbox);
		hbox.setOnMouseClicked(e -> {
			Inventory.Item item = this.inventory.new Item("Name", 1);
			this.inventory.addItem(item);
			HBox itemBox = createItem(item);
			itemBox.getChildren().get(0).requestFocus();
			this.inventoryList.getChildren().add(this.inventoryList.getChildren().size(), itemBox);
		});
		
		initInventoryList();
	}
	
	public void initInventoryList() {
		Platform.runLater(() -> this.inventoryList.getChildren().clear());
		List<Node> list = new ArrayList<Node>();
		for (Inventory.Item item : this.inventory.getInventoryList()) {
			HBox itemBox = createItem(item);
			list.add(itemBox);
		}
		Platform.runLater(() ->
			this.inventoryList.getChildren().addAll(list));
	}
	
	public HBox createItem(Inventory.Item item) {
		HBox hbox = new HBox(10);
		
		TextField name = new TextField(item.getName());
		name.setPadding(new Insets(10));
		name.setPrefWidth(500);
		name.setFont(Font.font(24));
		//name.setDisable(true);
		name.setBackground(null);
		name.textProperty().addListener((obs, old, nw) -> {
			item.setName(name.getText());
		});
		
		TextField quantity = new TextField(String.valueOf(item.getQuantity()));
		quantity.setPrefWidth(500);
		quantity.setPadding(new Insets(10));
		quantity.setFont(Font.font(24));
		//quantity.setDisable(true);
		quantity.setBackground(null);
		quantity.textProperty().addListener((obs, old, nw) -> {
			try {
				int qty = Integer.parseInt(nw);
				if (qty < 0) throw new Exception();
				item.setQuantity(qty);
				if (quantity.getStyleClass().contains("invalid"))
					quantity.getStyleClass().remove("invalid");
			}
			catch (Exception ex) {
				if (!quantity.getStyleClass().contains("invalid"))
					quantity.getStyleClass().add("invalid");
			}
		});
		
		Hyperlink remove = new Hyperlink("X");
		remove.setTextFill(Color.color(237.0/255.0, 60/255.0, 16/255.0));
		remove.setFont(Font.font(24));
		remove.setOnAction(e -> {
			this.inventory.removeItem(item);
			this.inventoryList.getChildren().remove(hbox);
			this.selectedItem = null;
			initLoanItem();
			initLoanMenu();
		});
		
		hbox.getChildren().addAll(name, quantity, remove);
		hbox.setOnMouseClicked(e -> {
			this.selectedItem = item;
			this.selectedItemHBox = hbox;
			this.manageLoansPane.setExpanded(true);
			initLoanItem();
		});
		name.setOnMouseClicked(hbox.getOnMouseClicked());
		quantity.setOnMouseClicked(hbox.getOnMouseClicked());
		
		hbox.getStyleClass().add("listItem");
		return hbox;
	}
	
	public void initLoanMenu() {
		this.manageLoansPane.setExpanded(false);
		this.manageLoansPane.setDisable(true);
	}
	
	public void initLoanItem() {
		this.manageLoansPane.setDisable(false);
		if (this.selectedItem != null) {
			this.itemName.textProperty().bind(
				new SimpleStringProperty("Name: ").concat(this.selectedItem.nameProperty())
			);
			this.itemQuantity.textProperty().bind(
				new SimpleStringProperty("Quantity: ").concat(this.selectedItem.quantityProperty())
			);
			this.itemLoaned.textProperty().bind(
				new SimpleStringProperty("Loaned: ").concat(this.selectedItem.loanedProperty())
			);
		}
		else {
			this.itemName.textProperty().unbind();
			this.itemQuantity.textProperty().unbind();
			this.itemLoaned.textProperty().unbind();
			this.itemName.setText("Name: ");
			this.itemQuantity.setText("Quantity: ");
			this.itemLoaned.setText("Loaned: ");
		}
	}
	
	@FXML public void loanItem() {
		selectedItem.loanItem();
		((TextField)selectedItemHBox.getChildren().get(1)).setText(String.valueOf(selectedItem.getQuantity()));
	}
	
	@FXML public void returnItem() {
		selectedItem.returnItem();
		((TextField)selectedItemHBox.getChildren().get(1)).setText(String.valueOf(selectedItem.getQuantity()));
	}
	
	@FXML public void loadInventory() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open an inventory file...");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("CSV Files", "*.csv")
		);
		File selectedFile = fileChooser.showOpenDialog(Main.getApplication().getWindowManager().getPrimaryStage());
		if (selectedFile != null) {
			Task<Void> task = new Task<Void>() {
				@Override 
				public Void call() {
					Scanner in = null;
					try {
						in = new Scanner(new BufferedReader(new FileReader(selectedFile)));
						in.useDelimiter(",+|\\n+");
						
						inventory.clear();
						
						String line;
						while (in.hasNextLine()) {
							line = in.nextLine();
							StringTokenizer tok = new StringTokenizer(line, ",");
							for (int i = 0; tok.hasMoreTokens(); ++i) {
								String name = tok.nextToken().trim();
								int quantity = Integer.parseInt(tok.nextToken().trim());
								inventory.addItem(inventory.new Item(name, quantity));
								this.updateMessage(String.format("Loading %d...", i));
							}
						}
						
						initInventoryList();
						
						Platform.runLater(() -> setLoadedFile(selectedFile));
					}
					catch (IOException | NoSuchElementException | NumberFormatException ex) {
						Platform.runLater(() -> {
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("An error has occurred");
							alert.setHeaderText("We were unable to load the file");
							String content = "The selected file may be corrupted or of the wrong format.";
							alert.setContentText(content);
					        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
					        alert.showAndWait();
						});
					}
					catch (Exception ex) {
						Platform.runLater(() -> {
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("An error has occurred");
							alert.setHeaderText("An unexpected error has occurred.");
							String content = "The details of an incident has been saved in a log file."
									+ " Please contact the developer with the message";
							alert.setContentText(content);
					        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
					        alert.showAndWait();
					        
					        Main.logException(ex);
						});
					}
					finally {
						if (in != null) in.close();
					}
					
					return null;
				}
			};
			
			progress.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
			leftStatus.textProperty().bind(task.messageProperty());
			task.stateProperty().addListener((observableValue, oldState, newState)-> {
				if (newState == Worker.State.SUCCEEDED) {
					leftStatus.textProperty().unbind();
					progress.setVisible(false);
					leftStatus.setText(null);
				}
			});
			
			new Thread(task).start();
		}
	}
	
	private void saveInventoryToFile(File file) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			
			for (Inventory.Item item : this.inventory.getInventoryList()) {
				out.println(item.getName() + "," + item.getQuantity());
			}
		}
		catch (IOException ex) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("An error has occurred");
			alert.setHeaderText("We were unable to save your file");
			String content = "Please try again.";
			alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
		}
		finally {
			if (out != null) out.close();
		}
	}
	
	@FXML public void saveInventory() {
		if (this.loadedFile == null) saveInventoryAs();
		else saveInventoryToFile(this.loadedFile);
	}
	
	@FXML public void saveInventoryAs() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialFileName("*.csv");
		fileChooser.setTitle("Save as an inventory file...");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("CSV Files", "*.csv")
		);
		File selectedFile = fileChooser.showSaveDialog(Main.getApplication().getWindowManager().getPrimaryStage());
		if (selectedFile != null) {
			saveInventoryToFile(selectedFile);
			this.setLoadedFile(selectedFile);
		}
	}
	
	private void setLoadedFile(File file) {
		this.loadedFile = file;
		this.loadedFileDisplay.setText("Loaded " + file.getName());
	}
	
	@FXML public void sortInventory() {
		try {
			ArrayList<Node> clone = new ArrayList<Node>(this.inventoryList.getChildren());
			clone.sort((Node a, Node b) -> {
				return ((TextField)((HBox)a).getChildren().get(0)).getText().compareTo(
						((TextField)((HBox)b).getChildren().get(0)).getText());
			});
			this.inventoryList.getChildren().clear();
			this.inventoryList.getChildren().addAll(clone);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public Widget getWidget() {
		File data = super.getTool().getData();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(data)));
			
			List<Integer> values = new ArrayList<Integer>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				StringTokenizer tok = new StringTokenizer(line);
				for (int i = 0; tok.hasMoreTokens(); ++i) {
					values.add(Integer.parseInt(tok.nextToken().trim()));
				}
			}
			
			final NumberAxis xAxis = new NumberAxis();
	        final NumberAxis yAxis = new NumberAxis();
	        xAxis.setLabel("Number of launches");
	        //creating the chart
	        final LineChart<Number,Number> lineChart = 
	                new LineChart<Number,Number>(xAxis,yAxis);
	                
	        lineChart.setTitle("Inventory Manager Usage");
	        //defining a series
	        XYChart.Series series = new XYChart.Series();
	        series.setName("Time spent in tool (min)");
	        //populating the series with data
	        for (int i = 0; i < values.size(); ++i) {
	        	series.getData().add(new XYChart.Data(i+1, values.get(i)));
	        }
	        lineChart.getData().add(series);
	        
	        return new Widget(lineChart);
	        
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (scanner != null) scanner.close();
		}
		return null;
	}
	
	@Override
	@FXML public void quitTool() {
		this.endTime = System.nanoTime();
		
		File data = super.getTool().getData();
		PrintWriter pr = null;
		try {  
			pr = new PrintWriter(new BufferedWriter(new FileWriter(data, true)));
			pr.print(" " + (int)Math.round((this.endTime - this.startTime) / 1000000000.0 / 60.0) );
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (pr != null) pr.close();
		}
		
		super.quitTool();
	}
}
