package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;
import models.InvalidTemplateException;
import models.TableData;
import models.Timeslot;
import models.Timetable;
import views.Widget;
import javafx.scene.layout.GridPane;

public class Tool_Manage_ManpowerController extends ToolController {
	@FXML private Button backBtn;
	@FXML private Button logoutBtn;
	private Timetable timetable;
	@FXML TableView<List<Object>> timetableView;
	@FXML private Button loadBtn;
	@FXML private Button saveBtn;
	@FXML private Button saveAsBtn;
	@FXML private TextField newColumns;
	@FXML private TextField newRows;
	@FXML private Button createNewTableBtn;
	@FXML private Button addNewColumnsBtn;
	@FXML private Button addNewRowsBtn;
	@FXML private ComboBox<Timetable.Algorithm> allocationType;
	@FXML private Button generateAllocationsBtn;
	@FXML private Label generateAllocationsTime;
	@FXML private Button loadOneDriveBtn;
	@FXML private Label loadOneDriveBtnWrapper;
	
	private int allocationsMade;
	
	public Tool_Manage_ManpowerController() {
		timetable = new Timetable();
	}
	
	@FXML public void initialize() {
		if (!Main.getApplication().getAuthenticator().isLoggedIn()) {
			this.loadOneDriveBtn.setDisable(true);
			this.loadOneDriveBtnWrapper.setTooltip(new Tooltip("Log in to access this tool"));
		}
		
		this.allocationsMade = 0;
		
		allocationType.setStyle("-fx-font-size: 18px");
		allocationType.getItems().addAll(
				Timetable.Algorithm.ONE_TO_ONE,
				Timetable.Algorithm.ONE_TO_TWO,
				Timetable.Algorithm.TWO_TO_ONE
		);
		allocationType.getSelectionModel().select(0);
	}
	
	private void initializeTable() {
		timetableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		timetableView.setSelectionModel(null);
		timetableView.setStyle("-fx-font-size: 20");
		
		ObservableList<List<Object>> data = FXCollections.observableArrayList(timetable.asList());
		data.remove(0);	// get rid of headers
		
		TableColumn<List<Object>, String> rh = new TableColumn<List<Object>, String>();
        rh.setCellValueFactory(new Callback<CellDataFeatures<List<Object>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<List<Object>, String> p) {
            	return new SimpleStringProperty((String)p.getValue().get(0));
            }
        });
        //rh.setPrefWidth(150);
        timetableView.getColumns().clear();
        timetableView.getColumns().add(rh);
		
        for (int i = 0; i < timetable.getColumns(); i++) {
            TableColumn<List<Object>, String> tc = new TableColumn<List<Object>, String>(timetable.getColumnHeaders().get(i));
            final int colNo = i+1;
            /*tc.setCellValueFactory(new Callback<CellDataFeatures<List<Object>, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(CellDataFeatures<List<Object>, String> p) {
                    return new SimpleStringProperty(((Timeslot)p.getValue().get(colNo)).isAllocated() ? "1" : "0");
                }
            });*/
            tc.setCellFactory(new Callback<TableColumn<List<Object>, String>, TableCell<List<Object>, String>>() {
            	public TableCell<List<Object>, String> call(TableColumn<List<Object>, String> p) {
                    TableCell<List<Object>, String> cell = new TableCell<List<Object>, String>() {	
                    	@Override
                        public void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (!empty) {
                            	TableRow<List<Object>> row = getTableRow();
                                List<Object> list = row == null ? null : (List<Object>)row.getItem();
                                if (list != null) {
                                	Timeslot slot = (Timeslot)list.get(colNo);
                                    
                                	setText(slot.isAllocated() ? "ASSIGNED" : "");
                                	setStyle(String.format("-fx-background-color: %s;", (slot.isAvailable() ? "white" : "red")));
                                }
                            }
                        }
                    };
                    
                    cell.setOnMouseClicked(e -> {
                    	TableRow<List<Object>> row = cell.getTableRow();
                        List<Object> list = row == null ? null : (List<Object>)row.getItem();
                        if (list != null) {
                        	Timeslot slot = (Timeslot)list.get(colNo);

                        	if (e.getButton() == MouseButton.PRIMARY) {
                        		slot.setAvailable(!slot.isAvailable());
                            	cell.setStyle(String.format("-fx-background-color: %s;", (slot.isAvailable() ? "white" : "red")));
                        	}
                        	else {
                        		slot.setAllocated(!slot.isAllocated());
                            	cell.setText(slot.isAllocated() ? "ASSIGNED" : "");
                        	}
                        }
                    });
                    
                    cell.getStyleClass().add("tableCell");
                    return cell;
                }
            });
            
            //tc.setPrefWidth(150);
            timetableView.getColumns().add(tc);
        }
        timetableView.setItems(data);
	}
	
	@FXML private void loadOneDriveDocument() {
		// Query for user profile name
		Content content = null;
		try {
			String path = "/Book.xlsx";
			String sheet = "Availability";
			String payload = "https://graph.microsoft.com/v1.0/me/drive/root:" 
							+ path + ":/workbook/worksheets/" + sheet + "/tables";
			content = Request.Get(payload)
					.addHeader("Authorization", Main.getApplication().getAuthenticator().getAccessToken())
				    .execute().returnContent();
			if (content == null) return;
			
			JsonObject json = new JsonParser().parse(content.asString()).getAsJsonObject();
			JsonArray tables = json.get("value").getAsJsonArray();
			if (tables.size() != 1) throw new InvalidDocumentException();
			
			String table = tables.get(0).getAsJsonObject().get("name").getAsString();
			payload += "/" + table + "/columns";
			
			content = null;
			content = Request.Get(payload)
					.addHeader("Authorization", Main.getApplication().getAuthenticator().getAccessToken())
				    .execute().returnContent();
		} catch (IOException | InvalidDocumentException e) {
			// TODO Auto-generated catch block
			Main.logException(e);
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Oops!");
	        alert.setHeaderText("Unable to load file.");
	        String contentBody = "Please ensure that you have a file named \"Book.xlsx\" under your root OneDrive directory, with a sheet \"Availability\"";
	        alert.setContentText(contentBody);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
		}
		
		System.out.println("Response: " + content);
		if (content != null) {
			Timetable newTimetable = new Timetable();
			
			JsonObject json = new JsonParser().parse(content.asString()).getAsJsonObject();
			JsonArray table = json.get("value").getAsJsonArray();
			try {
				for (int col = 0; col < table.size(); ++col) {
					JsonElement column = table.get(col);
					JsonObject columnObject = column.getAsJsonObject();			
					String name = columnObject.get("name").getAsString();
					JsonArray values = columnObject.get("values").getAsJsonArray();
					System.out.println(name + ": " + values);
					
					if (col == 0) {
						for (int i = 0; i < values.size(); ++i) {
							if (i == 0) continue;
							newTimetable.addRow(values.get(i).getAsString());
						}
					}
					else {
						for (int i = 0; i < values.size(); ++i) {
							String cur = values.get(i).getAsString();
							if (i == 0) 
								newTimetable.addColumn(cur);
							else
								newTimetable.setElement(i-1, col-1, new Timeslot(Boolean.valueOf(cur), false));
						}
					}
				}
				
				// newTimetable has been created successfully
				this.timetable = newTimetable;
				initializeTable();
			} catch (Exception ex) {
				Main.logException(ex);
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Oops!");
		        alert.setHeaderText("Unable to load file.");
		        String contentBody = "Please ensure that you have a file named \"Book.xlsx\" under your root OneDrive directory, with a sheet \"Availability\"";
		        alert.setContentText(contentBody);
		        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		        alert.showAndWait();
			}
		}
	}
	
	@FXML public void loadAvailability() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open a manpower availability file...");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("CSV Files", "*.csv")
		);
		File selectedFile = fileChooser.showOpenDialog(Main.getApplication().getWindowManager().getPrimaryStage());
		if (selectedFile != null) {
			Scanner in = null;
			try {
				in = new Scanner(new BufferedReader(new FileReader(selectedFile)));
				in.useDelimiter(",+|\\n+");
				
				String line;
				StringTokenizer tok;
				
				ArrayList<String> rowHeaders = new ArrayList<String>();
				line = in.nextLine();
				System.out.println(line);
				tok = new StringTokenizer(line, ",");
				while (tok.hasMoreTokens()) {
					String s = tok.nextToken().trim();
					rowHeaders.add(s);
				}
				
				ArrayList<String> columnHeaders = new ArrayList<String>();
				line = in.nextLine();
				tok = new StringTokenizer(line, ",");
				while (tok.hasMoreTokens()) {
					String s = tok.nextToken().trim();
					columnHeaders.add(s);
				}
				
				Timetable newTimetable = new Timetable();
				newTimetable.setRowHeaders(rowHeaders);
				newTimetable.setColumnHeaders(columnHeaders);
				
				for (int i = 0; i < newTimetable.getRows(); ++i) {
					for (int j = 0; j < newTimetable.getRow(i).size(); ++j) {
						String s = in.next().trim();
						Timeslot slot = new Timeslot(Boolean.parseBoolean(s), false);
						newTimetable.setElement(i, j, slot);
					}
				}
				
				// newTimetable has been created successfully
				this.timetable = newTimetable;
				initializeTable();
			}
			catch (IOException | NoSuchElementException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("An error has occurred");
				alert.setHeaderText("We were unable to load the file");
				String content = "The selected file may be corrupted or of the wrong format.";
				alert.setContentText(content);
		        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		        alert.showAndWait();
			}
			catch (Exception ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("An error has occurred");
				alert.setHeaderText("An unexpected error has occurred.");
				String content = "The details of an incident has been saved in a log file."
						+ " Please contact the developer with the message";
				alert.setContentText(content);
		        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		        alert.showAndWait();
		        
		        Main.logException(ex);
			}
			finally {
				if (in != null) in.close();
			}
		}
	}
	
	@FXML public void saveAvailability() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialFileName("*.csv");
		fileChooser.setTitle("Save as a manpower availability file...");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("CSV Files", "*.csv")
		);
		File selectedFile = fileChooser.showSaveDialog(Main.getApplication().getWindowManager().getPrimaryStage());
		if (selectedFile != null) {
			PrintWriter out = null;
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter(selectedFile)));
				
				for (int i = 0; i < this.timetable.getRowHeaders().size(); ++i) {
					if (i > 0) out.print(",");
					out.print(this.timetable.getRowHeaders().get(i));
				} out.println();
				
				for (int i = 0; i < this.timetable.getColumnHeaders().size(); ++i) {
					if (i > 0) out.print(",");
					out.print(this.timetable.getColumnHeaders().get(i));
				} out.println();
				
				for (int i = 0; i < this.timetable.getRows(); ++i) {
					for (int j = 0; j < this.timetable.getRow(i).size(); ++j) {
						if (j > 0) out.print(",");
						out.print(this.timetable.getRow(i).get(j).isAvailable());
					} out.println();
				}
			}
			catch (IOException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("An error has occurred");
				alert.setHeaderText("We were unable to save your file");
				String content = "Please try again.";
				alert.setContentText(content);
		        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		        alert.showAndWait();
			}
			finally {
				if (out != null) out.close();
			}
		}
	}
	
	@FXML public void saveAllocations() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialFileName("*.csv");
		fileChooser.setTitle("Save as a manpower allocations file...");
		fileChooser.getExtensionFilters().add(
				new ExtensionFilter("CSV Files", "*.csv")
		);
		File selectedFile = fileChooser.showSaveDialog(Main.getApplication().getWindowManager().getPrimaryStage());
		if (selectedFile != null) {
			PrintWriter out = null;
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter(selectedFile)));
				
				for (int i = 0; i < this.timetable.getRowHeaders().size(); ++i) {
					if (i > 0) out.print(",");
					out.print(this.timetable.getRowHeaders().get(i));
				} out.println();
				
				for (int i = 0; i < this.timetable.getColumnHeaders().size(); ++i) {
					if (i > 0) out.print(",");
					out.print(this.timetable.getColumnHeaders().get(i));
				} out.println();
				
				for (int i = 0; i < this.timetable.getRows(); ++i) {
					for (int j = 0; j < this.timetable.getRow(i).size(); ++j) {
						if (j > 0) out.print(",");
						out.print(this.timetable.getRow(i).get(j).isAllocated());
					} out.println();
				}
			}
			catch (IOException ex) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("An error has occurred");
				alert.setHeaderText("We were unable to save your file");
				String content = "Please try again.";
				alert.setContentText(content);
		        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		        alert.showAndWait();
			}
			finally {
				if (out != null) out.close();
			}
		}
	}
	
	@FXML public void createNewTable() {
		List<String> columnHeaders = new ArrayList<>(Arrays.asList(this.newColumns.getText().split(",")));
		columnHeaders.forEach(s -> s = s.trim());
		columnHeaders.removeIf(s -> {
			return s.equals("");
		});
		List<String> rowHeaders = new ArrayList<>(Arrays.asList(this.newRows.getText().split(",")));
		rowHeaders.forEach(s -> s = s.trim());
		rowHeaders.removeIf(s -> {
			return s.equals("");
		});
		
		if (columnHeaders.isEmpty()) { 
			if (!this.newColumns.getStyleClass().contains("invalid"))
				this.newColumns.getStyleClass().add("invalid");
		}
		else {
			if (this.newColumns.getStyleClass().contains("invalid"))
				this.newColumns.getStyleClass().remove("invalid");
		}
		if (rowHeaders.isEmpty()) { 
			if (!this.newRows.getStyleClass().contains("invalid"))
				this.newRows.getStyleClass().add("invalid");
		}
		else {
			if (this.newRows.getStyleClass().contains("invalid"))
				this.newRows.getStyleClass().remove("invalid");
		}
		
		if (columnHeaders.isEmpty() || rowHeaders.isEmpty()) return;
		
		this.timetable.setColumnHeaders(columnHeaders);
		this.timetable.setRowHeaders(rowHeaders);
		try {
			this.timetable.populate(this.timetable.getRows(), this.timetable.getColumns(), new Timeslot(false, false));
		} catch (InvalidTemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.initializeTable();
	}
	
	@FXML public void addNewColumns() {
		List<String> columnHeaders = new ArrayList<>(Arrays.asList(this.newColumns.getText().split(",")));
		columnHeaders.forEach(s -> s = s.trim());
		columnHeaders.removeIf(s -> {
			return s.equals("");
		});
		if (columnHeaders.isEmpty()) { 
			if (!this.newColumns.getStyleClass().contains("invalid"))
				this.newColumns.getStyleClass().add("invalid");
		}
		else {
			if (this.newColumns.getStyleClass().contains("invalid"))
				this.newColumns.getStyleClass().remove("invalid");
		}
		
		if (this.timetable.getRows() == 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Oops!");
	        alert.setHeaderText("Table is currently empty.");
	        String content = "Please ensure the table have at least 1 row before continuing.";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
	        return;
		}
		if (columnHeaders.isEmpty()) return;
		
		int startRow = 0;
		int startCol = this.timetable.getColumns()-1;
		this.timetable.addColumnHeaders(columnHeaders);
		int endRow = this.timetable.getRows();
		int endCol = this.timetable.getColumns();
		System.out.printf("%d %d %d %d\n", startRow, startCol, endRow, endCol);
		try {
			this.timetable.populate(startRow, startCol, endRow, endCol, new Timeslot(false, false));
		} catch (InvalidTemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initializeTable();
	}
	
	@FXML public void addNewRows() {
		List<String> rowHeaders = new ArrayList<>(Arrays.asList(this.newRows.getText().split(",")));
		rowHeaders.forEach(s -> s = s.trim());
		rowHeaders.removeIf(s -> {
			return s.equals("");
		});
		if (rowHeaders.isEmpty()) { 
			if (!this.newRows.getStyleClass().contains("invalid"))
				this.newRows.getStyleClass().add("invalid");
		}
		else {
			if (this.newRows.getStyleClass().contains("invalid"))
				this.newRows.getStyleClass().remove("invalid");
		}

		if (this.timetable.getColumns() == 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Oops!");
	        alert.setHeaderText("Table is currently empty.");
	        String content = "Please ensure the table have at least 1 column before continuing.";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
	        return;
		}
		if (rowHeaders.isEmpty()) return;
		
		int startRow = this.timetable.getRows()-1;
		int startCol = 0;
		this.timetable.addRowHeaders(rowHeaders);
		int endRow = this.timetable.getRows();
		int endCol = this.timetable.getColumns();
		try {
			this.timetable.populate(startRow, startCol, endRow, endCol, new Timeslot(false, false));
		} catch (InvalidTemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initializeTable();
	}
	
	@FXML public void generateAllocations() {
		Timetable.Algorithm selectedType = this.allocationType.getSelectionModel().getSelectedItem();
		
		Task<TableData<Boolean>> allocsTask = new Task<TableData<Boolean>>() {
			@Override
			public TableData<Boolean> call() {
				long startTime = System.nanoTime();
				TableData<Boolean> allocs = timetable.generateAllocations(selectedType);
				long endTime = System.nanoTime();
				String runTime = String.format("%.3f", (endTime-startTime)/1000000000.0);
				Platform.runLater(() -> {
					generateAllocationsTime.setText("Time taken: " + runTime);
				});
				return allocs;
			}
		};
		new Thread(allocsTask).start();
		
		TableData<Boolean> allocations;
		try {
			allocations = allocsTask.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			Main.logException(e);
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("An error has occurred.");
	        alert.setHeaderText("We were unable to generate the requested allocations.");
	        String content = "The error has been recorded in a log file.";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
			return;
		}
		
		for (int person = 0; person < allocations.getRows(); ++person) {
			for (int slot = 0; slot < allocations.getColumns(); ++slot) {
				this.timetable.getElement(person, slot).setAllocated(allocations.getElement(person, slot));
				System.out.print(allocations.getElement(person, slot) + " ");
			}
			System.out.println();
		}
		this.timetableView.refresh();
		++this.allocationsMade;
	}

	@Override
	public Widget getWidget() {
		// TODO Auto-generated method stub
		File data = super.getTool().getData();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(data)));
			
			List<Integer> values = new ArrayList<Integer>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				StringTokenizer tok = new StringTokenizer(line);
				for (int i = 0; tok.hasMoreTokens(); ++i) {
					values.add(Integer.parseInt(tok.nextToken().trim()));
				}
			}
			
			final NumberAxis xAxis = new NumberAxis();
	        final NumberAxis yAxis = new NumberAxis();
	        xAxis.setLabel("Number of launches");
	        //creating the chart
	        final LineChart<Number,Number> lineChart = 
	                new LineChart<Number,Number>(xAxis,yAxis);
	                
	        lineChart.setTitle("Manpower Manager Usage");
	        //defining a series
	        XYChart.Series series = new XYChart.Series();
	        series.setName("Number of allocations made");
	        //populating the series with data
	        for (int i = 0; i < values.size(); ++i) {
	        	series.getData().add(new XYChart.Data(i+1, values.get(i)));
	        }
	        lineChart.getData().add(series);
	        
	        return new Widget(lineChart);
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (scanner != null) scanner.close();
		}
		return null;
	}
	
	@Override
	@FXML public void quitTool() {
		File data = super.getTool().getData();
		PrintWriter pr = null;
		try {  
			pr = new PrintWriter(new BufferedWriter(new FileWriter(data, true)));
			pr.print(" " + this.allocationsMade);
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (pr != null) pr.close();
		}
		
		super.quitTool();
	}
}
