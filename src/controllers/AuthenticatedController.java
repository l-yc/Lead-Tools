package controllers;

import application.Main;
import javafx.fxml.FXML;

public abstract class AuthenticatedController {
	@FXML public void logout() {
		Main.getApplication().logout();
	}
}
