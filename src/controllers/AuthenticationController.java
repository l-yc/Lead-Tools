package controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.conn.ConnectTimeoutException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Region;

public class AuthenticationController {
	private final String client_id = "1b637894-81c1-4c7b-88d8-196c1e20e11b";
	private final String response_type = "code";
	private final String redirect_uri = "https://login.microsoftonline.com/common/oauth2/nativeclient";
	private final String response_mode = "query";
	private final String scope = "offline_access user.readbasic.all user.read user.readwrite files.read files.readWrite calendars.read calendars.readwrite";
	private final int state = (int)Math.floor(Math.random() * 65536);
	
	private String authorizationCode = null;
	private final String grant_type_code = "authorization_code";
	private final String grant_type_token = "refresh_token";
	private String accessToken = null;
	private String refreshToken = null;
	private ScheduledExecutorService refreshTokenExecutor = null;
	
	private final String codeEndpoint = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?";
	private final String tokenEndpoint = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
	
	@FXML private Hyperlink loginLink;
	@FXML private TextField urlDisplay;
	@FXML private TextField returnUrl;

	@FXML public void initialize() throws UnsupportedEncodingException {
		String url = codeEndpoint
				+ "client_id=" + URLEncoder.encode(client_id, "UTF-8")
				+ "&response_type=" + URLEncoder.encode(response_type, "UTF-8")
				+ "&redirect_uri=" + URLEncoder.encode(redirect_uri, "UTF-8")
				+ "&response_mode=" + URLEncoder.encode(response_mode, "UTF-8")
				+ "&scope=" + URLEncoder.encode(scope, "UTF-8")
				+ "&state=" + String.valueOf(state);
		
		loginLink.setOnAction(e -> {
			Main.getApplication().getHostServices().showDocument(url);
		});
		
		urlDisplay.setText(url);
		
		returnUrl.setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.ENTER) this.authenticateUser();
		});
	}
	
	protected void parseAuthorizationCode() {
		String url = returnUrl.getText();
		/*Pattern pattern = Pattern.compile("https://login\\.microsoftonline\\.com/common/oauth2/nativeclient\\?code=([a-Z]|\\d|_|-)+&state=([a-Z]|\\d|_|-)+&session_state=([a-Z]|\\d|_|-)+");
		Matcher matcher = pattern.matcher(url);*/
		
		//if (matcher.matches()) {
		try {
			int start = url.indexOf('=');
			int end = url.indexOf('&');
			this.authorizationCode = url.substring(start+1, end);
			int state = Integer.parseInt(url.substring(url.indexOf('=', start+1)+1, url.indexOf('&', end+1)));
			if (state != this.state) throw new AuthenticationException();
			System.out.println("[Success] Obtained authorization code: " + this.authorizationCode);
		}
		catch (StringIndexOutOfBoundsException | NumberFormatException | AuthenticationException ex) {
		//else {
			this.authorizationCode = null;
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid Input");
	        alert.setHeaderText("Invalid return url entered.");
	        String content = "Please check your url and try again.";
	        alert.setContentText(content);
	        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
	        alert.showAndWait();
		}
	}
	
	protected void authenticateUser() {
		this.parseAuthorizationCode();
		if (this.authorizationCode == null) return;
		
		Task<Void> loginTask = new Task<Void>() {
			@Override
			public Void call() throws InterruptedException {
				try {
					System.out.println("Querying for token...");
					this.updateMessage("Querying for token...");
					this.updateProgress(0.5, 1.0);

					Content content = Request.Post(tokenEndpoint)
									    .bodyForm(Form.form()
									    		.add("client_id",  client_id)
									    		.add("scope",  scope)
									    		.add("code", authorizationCode)
									    		.add("grant_type", grant_type_code)
									    		.add("redirect_uri", redirect_uri)
									    		.build())
									    .connectTimeout(10000)
									    .execute().returnContent();
					System.out.println("Response: " + content);
					JsonObject json = new JsonParser().parse(content.asString()).getAsJsonObject();
					accessToken = json.get("access_token").getAsString();
					System.out.println("Obtained access token: " + accessToken);
					this.updateMessage("Obtained access token...");
					this.updateProgress(0.75, 1.0);
					
					refreshToken = json.get("refresh_token").getAsString();
					System.out.println("Obtained refresh token: " + refreshToken);
					this.updateMessage("Querying refresh token...");
					
					this.updateProgress(1.0, 1.0);
					Thread.sleep(200);
					this.updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 1.0);
					this.updateMessage("Done!");
					Thread.sleep(1200);
					
					Runnable refreshTask = () -> {
						Content content2 = null;
						try {
							content2 = Request.Post(tokenEndpoint)
											    .bodyForm(Form.form()
											    		.add("client_id",  client_id)
											    		.add("scope",  scope)
											    		.add("refresh_token", refreshToken)
											    		.add("grant_type", grant_type_token)
											    		.add("redirect_uri", redirect_uri)
											    		.build())
											    .connectTimeout(10000)
											    .execute().returnContent();
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("Response: " + content2);
						if (content2 != null) {
							JsonObject json2 = new JsonParser().parse(content2.asString()).getAsJsonObject();
							accessToken = json2.get("access_token").getAsString();
							System.out.println("Obtained access token: " + accessToken);
							
							refreshToken = json2.get("refresh_token").getAsString();
							System.out.println("Obtained refresh token: " + refreshToken);
						}
					};
					refreshTokenExecutor = Executors.newSingleThreadScheduledExecutor();
					refreshTokenExecutor.scheduleAtFixedRate(refreshTask, 50, 50, TimeUnit.MINUTES);
				}
				catch (ConnectTimeoutException | UnknownHostException ex) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("An error has occurred");
			        alert.setHeaderText("You have been disconnected");
			        String content = "Please check your network connection and try again.";
			        alert.setContentText(content);
			        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			        alert.showAndWait();
					this.cancel();
				}
				catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("Error Message: " + ex.getMessage());
					if (ex instanceof HttpResponseException)
						System.out.println("Error Code: " + ((HttpResponseException) ex).getStatusCode());
					
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("An error has occurred.");
			        alert.setHeaderText("We were unable to log you in.");
			        String content = "Please try again.";
			        alert.setContentText(content);
			        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			        alert.showAndWait();
					this.cancel();
				}
				catch (Exception ex) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("An unexpected error has occurred.");
			        alert.setHeaderText("We were unable to log you in.");
			        String content = "The error has been recorded in a log file. Please try again.";
			        alert.setContentText(content);
			        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			        alert.showAndWait();
			        Main.logException(ex);
			        this.cancel();
				}
				return null;
			}
		};
		
		Main.getApplication().login(loginTask);
	}
	
	@FXML public void continueWithoutLogin() {
		//this.accessToken = "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFEWHpaM2lmci1HUmJEVDQ1ek5TRUZFdF91VEprRVhqNUJ5STkzdlk0SmxMUm1maHh0TkZIU0VkaVpJMUI1WnJjZEdqemVqNjBlVHMwbC1ZcVRpZ2FaUWhPMUxpdU15WTN2S1BiRkFEWlp1UkNBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoiaTZsR2szRlp6eFJjVWIyQzNuRVE3c3lISmxZIiwia2lkIjoiaTZsR2szRlp6eFJjVWIyQzNuRVE3c3lISmxZIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9kNzJhNzE3Mi1kNWY4LTQ4ODktOWE4NS1kNzQyNDc1MTU5MmEvIiwiaWF0IjoxNTM4ODM1ODEyLCJuYmYiOjE1Mzg4MzU4MTIsImV4cCI6MTUzODgzOTcxMiwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkFTUUEyLzhJQUFBQWJjczYzUWswMW81UWNzR1pUV2JVekVWNTE0Q296TlNaTXhGK3JmbVZpS1k9IiwiYW1yIjpbInB3ZCJdLCJhcHBfZGlzcGxheW5hbWUiOiJDUzMyMzMgUHJvamVjdCIsImFwcGlkIjoiMWI2Mzc4OTQtODFjMS00YzdiLTg4ZDgtMTk2YzFlMjBlMTFiIiwiYXBwaWRhY3IiOiIwIiwiZ2l2ZW5fbmFtZSI6IkxJIFlVRSBDSEVOIiwiaXBhZGRyIjoiMTE1LjY2LjI0MS4xOTgiLCJuYW1lIjoiTEkgWVVFIENIRU4iLCJvaWQiOiI5ODI4ODlkZC02M2JkLTQ2OTAtODZjMC1kYmUyZmZiZjljMWEiLCJwbGF0ZiI6IjE0IiwicHVpZCI6IjEwMDNCRkZEOTU3OUI3OTEiLCJzY3AiOiJDYWxlbmRhcnMuUmVhZCBDYWxlbmRhcnMuUmVhZFdyaXRlIEZpbGVzLlJlYWQgRmlsZXMuUmVhZFdyaXRlIFVzZXIuUmVhZCBVc2VyLlJlYWRXcml0ZSBwcm9maWxlIG9wZW5pZCBlbWFpbCIsInN1YiI6IjFpbnpTT3ZPRFpnSVZEVGpsa0dqYXNIUVkwREw5MTRkVDFuazZFZDAwT0EiLCJ0aWQiOiJkNzJhNzE3Mi1kNWY4LTQ4ODktOWE4NS1kNzQyNDc1MTU5MmEiLCJ1bmlxdWVfbmFtZSI6ImgxNjEwMDczQG51c2hpZ2guZWR1LnNnIiwidXBuIjoiaDE2MTAwNzNAbnVzaGlnaC5lZHUuc2ciLCJ1dGkiOiIyYzEzLVhGZzQwYWY4WUtqWDRsdUFBIiwidmVyIjoiMS4wIiwieG1zX3N0Ijp7InN1YiI6Ild3aElQUXpTQmdvMUxpb3prLXVTZHpEUmVvYjlmNXhLWjR0X0ZQQzZacGcifSwieG1zX3RjZHQiOjEzNjYzOTQwNjV9.kbogVwjGIvSZ-M9QVum3mr8UfBowuMik89FI09IPrQHZncRvXbiXantuyKNCyhwRlrT_DuVv3QV7HISzuUrdSNfs6TgJs4FjQI4dD10lHa6dfz4cGqzqQV3yLtIRJEbghj6PAUniLiVdkl3-1a3IqNptPj7ytZNA4_VmtIuGAJ5KMh_VQ_ENf8X_t1K5uo1reofRiNuC6PA4LWhw1pW_yK0pcvxLtNowvNqP7kzQSB_FBbH74I3Q43SAO_UMe7fwFL9QLYon5wpTjt3OXNvsSsqTwHkoqUH1vdZoiE3P18rsLMC5dqgDTJ2fM7l2pF73zV5EF2cPF9w1Cyh07z1MBw";
		Main.getApplication().continueWithoutLogin();
	}
	
	public String getAccessToken() {
		return this.accessToken;
	}

	public boolean isLoggedIn() {
		// TODO Auto-generated method stub
		return this.accessToken != null;
	}
}
