package controllers;

import java.io.File;

import javafx.scene.Parent;

public class Tool {
	private final String category;
	private final String name;
	private final Parent parent;
	private final ToolController controller;
	private final File data;
	
	public Tool(String category, String name, Parent parent, ToolController controller, File data) {
		this.category = category;
		this.name = name;
		this.parent = parent;
		this.controller = controller;
		this.data = data;
	}

	public String getCategory() {
		return category;
	}
	
	public String getName() {
		return name;
	}
	
	public Parent getParent() {
		return parent;
	}

	public ToolController getController() {
		return controller;
	}

	public File getData() {
		return data;
	}
	
	@Override
	public String toString() {
		return "(" + this.category + ") " + this.name;
	}
}
