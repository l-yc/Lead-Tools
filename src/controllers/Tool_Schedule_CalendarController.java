package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import application.Main;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import views.Widget;

public class Tool_Schedule_CalendarController extends ToolController {
	@FXML private Button backBtn;
	@FXML private Button logoutBtn;
	@FXML private GridPane calendarView;
	private VBox[][] calendarViewNodes;
	private int[] weekOfDay;
	@FXML Label eventPreview;
	
	private long startTime, endTime;
	
	public Tool_Schedule_CalendarController() {
	}
	
	@FXML public void initialize() {
		this.startTime = System.nanoTime();
		
		calendarView.getChildren().clear();
		calendarView.getStyleClass().add("calendar");
		calendarViewNodes = new VBox[6][7];
		weekOfDay = new int[32];
		LocalDate today = LocalDate.now();
		LocalDate startDate = today.minusDays(today.getDayOfMonth() - 1);
		LocalDate endDate = startDate.plusDays(today.getMonth().length(today.isLeapYear()) - 1);
		int row = -1;
		for (int i = 0; i < today.getMonth().length(today.isLeapYear()); ++i) {
			int cur = startDate.plusDays(i).getDayOfWeek().getValue();
			Label label = new Label(String.valueOf(i+1));
			label.setFont(Font.font(null, FontWeight.BOLD, 24));
			VBox vbox = new VBox(10, label);
			vbox.setMinSize(160, 100);
			vbox.getStyleClass().add("calendarCell");
			GridPane.setHgrow(vbox, Priority.ALWAYS);
			if (cur == 1 || row < 0) {
				++row;
				for (int c = 0; c < 7; ++c) {
					if (c == cur) continue;
					VBox cell = new VBox(10);
					cell.setMinSize(160, 100);
					cell.getStyleClass().add("calendarCellDisabled");
					GridPane.setHgrow(cell, Priority.ALWAYS);
					calendarView.add(cell, c, row);
				}
			}
			calendarView.add(vbox, cur-1, row);
			calendarViewNodes[row][cur-1] = vbox;
			weekOfDay[i] = row;
		}
		for (ColumnConstraints c : calendarView.getColumnConstraints()) {
            c.setPercentWidth(100.0 / calendarView.getColumnConstraints().size());
        }
		for (RowConstraints r : calendarView.getRowConstraints()) {
            r.setPercentHeight(100.0 / calendarView.getRowConstraints().size());
        }
		
		// Get the ISO Strings
		String endpoint = "https://graph.microsoft.com/v1.0/me/calendarview?";
		String args = "startdatetime=" + startDate + "&enddatetime=" + endDate;
		
		Content content = null;
		try {
			content = Request.Get(endpoint + args)
					.addHeader("Authorization", Main.getApplication().getAuthenticator().getAccessToken())
				    .execute().returnContent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Cannot get calendar");
			Main.logException(e);
		}
		
		System.out.println("Response:" + content);
		if (content != null) {
			JsonObject json = new JsonParser().parse(content.asString()).getAsJsonObject();
			JsonArray events = json.get("value").getAsJsonArray();
		
			for (JsonElement e : events) {
				JsonObject event = e.getAsJsonObject();
				String subject = event.get("subject").getAsString();
				String bodyPreview = event.get("bodyPreview").getAsString();
				
				String dateString = event.get("start").getAsJsonObject().get("dateTime").getAsString() + "Z";
				DateTimeFormatter isoFormatter = DateTimeFormatter.ISO_INSTANT;
			    Instant dateInstant = Instant.from(isoFormatter.parse(dateString));
			    LocalDateTime startTime = LocalDateTime.ofInstant(dateInstant, ZoneId.of(ZoneOffset.UTC.getId()));
				
				int week = weekOfDay[startTime.getDayOfMonth()];
				int dayOfWeek = startTime.getDayOfWeek().getValue()-1;
				
				Label label = new Label(subject);
				label.setFont(Font.font(18));
				label.setTooltip(new Tooltip(bodyPreview));
				label.getStyleClass().add("event");
				label.setOnMouseClicked(ev -> {
					this.eventPreview.setText(bodyPreview);
				});
				calendarViewNodes[week][dayOfWeek].getChildren().add(label);
			}
		}
	}
	
	@Override
	public Widget getWidget() {
		File data = super.getTool().getData();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(data)));
			
			List<Integer> values = new ArrayList<Integer>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				StringTokenizer tok = new StringTokenizer(line);
				for (int i = 0; tok.hasMoreTokens(); ++i) {
					values.add(Integer.parseInt(tok.nextToken().trim()));
				}
			}
			
			final NumberAxis xAxis = new NumberAxis();
	        final NumberAxis yAxis = new NumberAxis();
	        xAxis.setLabel("Number of launches");
	        //creating the chart
	        final LineChart<Number,Number> lineChart = 
	                new LineChart<Number,Number>(xAxis,yAxis);
	                
	        lineChart.setTitle("Calendar Usage");
	        //defining a series
	        XYChart.Series series = new XYChart.Series();
	        series.setName("Time spent in tool (min)");
	        //populating the series with data
	        for (int i = 0; i < values.size(); ++i) {
	        	series.getData().add(new XYChart.Data(i+1, values.get(i)));
	        }
	        lineChart.getData().add(series);
	        
	        return new Widget(lineChart);
	        
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (scanner != null) scanner.close();
		}
		return null;
	}
	
	@Override
	@FXML public void quitTool() {
		this.endTime = System.nanoTime();
		
		File data = super.getTool().getData();
		PrintWriter pr = null;
		try {  
			pr = new PrintWriter(new BufferedWriter(new FileWriter(data, true)));
			pr.print(" " + (int)Math.round((this.endTime - this.startTime) / 1000000000.0 / 60.0) );
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (pr != null) pr.close();
		}
		
		super.quitTool();
	}
}
