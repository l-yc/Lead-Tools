package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

import application.Main;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import views.Widget;

public class __Tool_Schedule_MeetingController extends ToolController {
	@FXML private Button backBtn;
	@FXML private Button logoutBtn;
	
	public __Tool_Schedule_MeetingController() {
	}
	
	@FXML public void initialize() {
	}
	
	@Override
	public Widget getWidget() {
		File data = super.getTool().getData();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(data)));
			
			List<Integer> values = new ArrayList<Integer>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				StringTokenizer tok = new StringTokenizer(line);
				for (int i = 0; tok.hasMoreTokens(); ++i) {
					values.add(Integer.parseInt(tok.nextToken().trim()));
				}
			}
			
			final NumberAxis xAxis = new NumberAxis();
	        final NumberAxis yAxis = new NumberAxis();
	        xAxis.setLabel("Number of launches");
	        //creating the chart
	        final LineChart<Number,Number> lineChart = 
	                new LineChart<Number,Number>(xAxis,yAxis);
	                
	        lineChart.setTitle("Inventory Manager Usage");
	        //defining a series
	        XYChart.Series series = new XYChart.Series();
	        series.setName("Time spent in tool (min)");
	        //populating the series with data
	        for (int i = 0; i < values.size(); ++i) {
	        	series.getData().add(new XYChart.Data(i+1, values.get(i)));
	        }
	        lineChart.getData().add(series);
	        
	        return new Widget(lineChart);
	        
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (scanner != null) scanner.close();
		}
		return null;
	}
	
	@Override
	@FXML public void quitTool() {
		File data = super.getTool().getData();
		PrintWriter pr = null;
		try {  
			pr = new PrintWriter(new BufferedWriter(new FileWriter(data, true)));
		} 
		catch (IOException | NoSuchElementException | NumberFormatException ex) {
			// TODO Auto-generated catch block
			Main.logException(ex);
		}
		finally {
			if (pr != null) pr.close();
		}
		
		super.quitTool();
	}
}
